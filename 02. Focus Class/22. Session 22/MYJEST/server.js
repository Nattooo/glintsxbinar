const express = require('express');
const morgan = require('morgan');

const dotenv = require('dotenv')
dotenv.config();

const router = require('./router');;

// Keep the initialization after the import statement
const app = express();

// Basic Express Configuration
app.use(express.json());
app.use(router);
if (process.env.NODE_ENV !== 'test')
  app.use(morgan('dev'))

/* Export the app,
 * because we need the
 * server instance to run somewhere else
 * */
module.exports = app;