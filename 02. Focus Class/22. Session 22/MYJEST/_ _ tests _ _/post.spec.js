const request = require('supertest');
const app = require('../server');
const { Post } = require('../models');

describe('Post API Collection', () => {

    beforeAll(() => {
        return Post.destroy({
            truncate: true
        })
    })

    afterAll(() => {
        return Post.destroy({
            truncate: true
        })
    })

    describe('POST /api/v1/posts', () => {
        test('Should successfully create a new post', done => {
            request(app).post('/api/v1/posts')
                .set('Content-Type', 'application/json')
                .send({ tittle: 'You', body: 'Love is in the air'})
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    expect(res.body.data).toHaveProperty('post');
                    done();
                })
        })

        test('Should not create new post', done => {
            request(app)
                .post('/api/v1/posts')
                .set('Content-Type', 'application/json')
                .send({title: 'You'})
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
})
