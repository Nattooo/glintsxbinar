module.exports = (res, data, statusCode, key) => {
  res.status(statusCode).json({
    status: 'Success',
    data: {
      [key]: data
    }
  })
}  