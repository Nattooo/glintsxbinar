## Git ?

Git adalah suatu "version control". Version control sendiri digunakan untuk membantu web dev mengontrol perubahan yang berlaku terhadap file atau folder project.
Penggunaan version control mempermudah pengaturan project bila project tersebut dilakukan secara bersama-sama dalam sebuah tim. 
Sehingga masing-masing anggota tim dapat bekerja dengan device atau laptopnya masing-masing yang kemudian dapat disatukan dengan menggunakan version control.

## code-code git

Dalam penggunaan Git, pertama-tama kita harus initialize Git ke local repository atau laptop kita.
Setelah itu kita mengamankan interaksi dengan menggunakan SSH Key. Hal ini dilakukan untuk mengidentifikasi siapa yang melakukan perubahan.
Git memiliki Repository Master dapat membuat branch, jadi sebelum 

Membuat salinan ke repository local
git clone <url>

Menambahkan perubahan
git add <nama berkas>

Commit perubahan
git commit -m "<pesan commit>"
bila tidak memasukan -m maka akan masuk terminal baru
disitu kita dapat memasukan pesan
lalu tekan CTRL X lalu Shift Y lalu enter

mengirim perubahan ke repository remote
git push origin master
master = branch
origin = nama remote

Memperbarui repository lokal dari repository remote
git pull origin master
master = branch
origin = nama remote

menggabungkan cabang
git merge <cabang>

Mengembalikan perubahan
git checkout -- <nama berkas>

Membatalkan perubahan yang sudah di add namun belum di commit
git reset <nama file>

Melihat history perubahan
git log

Menambahkan branch
git branch <nama branch>

Menghapus branch 
git branch -D <nama branch>

Menunjukan remote di local directory
git remote -v

## VSC
untuk membuka terminal di VSC
tekan CTRL + '
atau CTRL + ~