const fs = require('fs');

class Record {
  constructor(props) {
    if (this.constructor == Record) 
      throw new Error("Can't instantiate from Record");
    
    this._validate(props);
    this._set(props);
  }

  _validate(props) {
    if (typeof props !== 'object' || Array.isArray(props))
      throw new Error("Props must be an object");
    

    this.constructor.properties.forEach(i => {
      if (!Object.keys(props).includes(i))
        throw new Error(`${this.constructor.name}: ${i} is required`)
    })
  }

  _set(props) {
    this.constructor.properties.forEach(i => {
      this[i] = props[i];
    })
  }

  get all() {
    try {
      return eval(
        fs.readFileSync(`${__dirname}/${this.constructor.name}.json`)
          .toString()
      )
    }
    catch {
      return []
    }
  }

  find(id) {

  }

  update(id) {

  }

  delete(id) {

  }

  save() {
    fs.writeFileSync(
      `${__dirname}/${this.constructor.name}.json`,
      JSON.stringify([...this.all, { id: this.all.length + 1, ...this } ], null, 2)
    );
  }
}


class Book extends Record {

    static properties = [
        "title",
        "author",
        "price",
        "publiser"
    ];

    constructor(props) {
      super(props);
    }

    super.save();
}

class Product extends Record {

    static properties = [
        "name",
        "price",
        "stock",
    ]

    constructor(props) {
        super(props);
    }

    super.save();
}

let Matahari = new Book ({
    title: "Matahari",
    author: "Chris",
    price: "104.000",
    publiser: "PT Edie"
});

let Bulan = new Book ({
    title: "Bulan",
    author: "Pita",
    price: "80.000",
    publiser: "PT Rinie"
})

let Dress = new Product ({
    name: "Pink Dress",
    price: "250.000",
    stock: "12"
})

let Sneaker = new Product ({
    name: "Converse white",
    price: "1.500.000",
    stock: "5" 
})

Matahari.save();
Bulan.save();
Dress.save();
Sneaker.save();