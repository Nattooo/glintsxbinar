const products = require('./products.json');
const user = require('./user.json');

const http = require('http');

function handler(req, res) {

  function present(data, fields = null) {
    res.write(JSON.stringify(data, fields));
    res.end();
  }
  
  if (req.url === '/products' && req.method == 'GET')
    present(products)
  else if (req.url === '/products/available' && req.method == 'GET')
    present(products.filter(i => i.stock > 0))
  else if (req.url === "/users" && req.method === "GET")
    present(user, ['id', 'name', 'email', 'role', 'verified'])
  else
    present("404 Not Found!");
}

http.createServer(handler)
  .listen(3000, () =>
    console.log("Server is running!")
  )