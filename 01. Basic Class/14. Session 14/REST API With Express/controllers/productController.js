const product = require('./db/product.json');

module.exports = {
  all(req, res) {
    res.status(200).json(product);
  },

  available(req, res) {
    res.status(200).json(product.filter(i => i.stock > 0))
  }
}