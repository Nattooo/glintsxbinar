const express = require('express');
const app = express()

const user = require('./db/user.json');
const router = require('./router.js');

// Middleware to Parse JSON
app.use(express.json());
 
// GET /
app.get('/', function (req, res) {
  res.send('Hello World')
})
app.use('/', router);

app.get('/user', (req, res) => {
  let entity = {...user};
  delete entity.password
  res.status(200).json(entity)
})

app.post('/login', (req, res) => {
  if (user.email !== req.body.email)
    return res.status(401).json({
      status: false,
      message: "Email doesn't exist"
    })

  if (user.password !== req.body.password)
    return res.status(401).json({
      status: false,
      message: "Wrong password!"
    })

  res.status(200).json({
    status: true,
    message: "Succesfully logged in"
  })
})
 
app.listen(3000, () => {
  console.log("Listening on port 3000!");
})