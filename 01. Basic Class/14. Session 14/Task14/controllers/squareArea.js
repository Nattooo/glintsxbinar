function squareArea(req, res) {
    let result = req.body.s**2;
    return res.status(200).json({
        status: true,
        result
    })
  }
  
  module.exports = squareArea;