function tubeVolume(req, res) {
    let result = 3.14*req.body.r**2*req.body.h;
    return res.status(200).json({
        status: true,
        result
    })
}

module.exports = tubeVolume;
