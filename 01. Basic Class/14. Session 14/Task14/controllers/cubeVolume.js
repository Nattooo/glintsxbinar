function cubeVolume(req, res) {
    let result = req.body.a**3;
    return res.status(200).json({
        status: true,
        result
    })
}

module.exports = cubeVolume;