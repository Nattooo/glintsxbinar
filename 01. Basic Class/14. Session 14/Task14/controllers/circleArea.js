function circleArea(req, res) {
    let result = 3.14*req.body.r**2;
    return res.status(200).json({
        status: true,
        result
    })
}

module.exports = circleArea;