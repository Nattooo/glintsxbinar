function triangleArea(req, res) {
    let result = 0.5*req.body.b*req.body.h;
    return res.status(200).json({
        status: true,
        result
    })
}

module.exports = triangleArea;

