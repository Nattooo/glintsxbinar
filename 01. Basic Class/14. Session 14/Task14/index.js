const express = require('express');
const app = express()
const port = 3000

const circleArea = require('./controllers/circleArea');
const cubeVolume = require('./controllers/cubeVolume');
const squareArea = require('./controllers/squareArea');
const triangleArea = require('./controllers/triangleArea');
const tubeVolume = require('./controllers/tubeVolume');

app.use(express.json());

app.get('/', function (req, res) {
    res.send('Hello World')
})

app.get('/info', function (req, res) {
    "status": true,
    "data": [
        "POST /calculate/circleArea",
        "POST /calculate/squareArea",
        "POST /calculate/cubeVolume",
        "POST /calculate/tubeVolume",
        "POST /calculate/triangleArea",
    ]
})


app.listen(3000, () => {
    console.log("Listening on port 3000!");
})