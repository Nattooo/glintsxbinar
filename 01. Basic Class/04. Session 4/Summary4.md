Create basic arithmatic, but it is on [another repository](https://gitlab.com/Nattooo/session-4-team-a)

# Session 4

We learn about how to collaborate using Git

### Here is the steps

1. A team member made Repository on Gitlab.
2. Added another member also mentor to the Repository as maintainer. We did it from repository setting - member.
3. Each member cloned the remote repository to their local repository.
4. Each member made their own branch. Mentor suggest that the name of the branch should interprate their own task. We made branch from our terminal 
'''
git checkout -b <branchName>
5. We made the task on our code editor such as Visual Studio Code. And save it.
6. we add and commit our task on the branch.
'''
git add <filename>
git commit -m "[userName]<message>"
'''
7. Push the task to branch
'''
git push origin <branchName>
'''
8. We should made merge request. if there are notification said "... commit behind", then we should update our branch data by updated it from master.
'''
git pull origin master
'''
9. Then see if there are anything to be commit, we should commit it. if not, then we are ready to push our task to master branch.
10. After checked that, we can merge our barnch to master branch.
11. Before we push, we should go to master branch first. Then, don't forget to add and commit first.
'''
git add <taskName>
git commit -m "[userName]<message>"
git push origin master
'''