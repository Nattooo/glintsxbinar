const product = require('../models').product;

module.exports = {
  all(req, res) {
    product.findAll().then(data =>
      res.status(200).json({
        status: true,
        data
      })
    )
  },

  findOne(req, res) {
    const id = req.params.id;

    product.findById(id).then(data =>
      res.status(200).json({
        status: true,
        data
      }))
  },

  available(req, res) {
    res.status(200).json(product.filter(i => i.stock > 0))
  },

  create(req, res) {
    const products = {
      name: req.body.name,
      price: req.body.price,
      stock: req.body.stock
    }
    product.create(products).then(data =>
      res.status(200).json({
        status: true,
        data
      })
    ).catch(err => {
      console.log(err.message);
      res.status(500).send({
        message: "Error occurred while creating the Product"
      })
    })
  },

  update(req, res) {
    const id = req.params.id;

    product.update(req.body, {
      where: { id: id}
    }).then(num => {
      if (num == 1) {
        res.send({
          message:"Product successfully updated"
        });
      } else {
        res.send({
          message:"Cannot update product with this id"
        });
      }
    }).catch(err => {
      res.status(500).send({
        message: "Error while updating with this id"
      });
    });
  },

  delete(req, res) {
    const id = req.params.id;

    product.destroy({
      where: { id: id}
    }).then(num => {
      if (num == 1) {
        res.send({
          message: "Product with this id deleted"
        });
      } else {
        res.send({
          message: "Cannot deleted product with this id"
        });
      }
    }).catch(err => {
      res.status(500).send({
        message: "Could not delete the product"
      });
    });
  }
}