const express = require('express');
const app = express()

//Middleware
const router = require('./router.js');

const cors = require('cors');

// Middleware to Parse JSON
app.use(express.json()); //Coba sendiri
 
// GET /
app.get('/', function (req, res) {
  res.json({
    status: true,
    message: "Hello World!"
  })
})

app.use('/', router);
app.use(cors());

app.listen(6000, () => {
  console.log("Listening on port 6000!");
})