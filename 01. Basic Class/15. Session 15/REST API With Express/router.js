const express = require('express');
const router = express.Router();
const product = require('./controllers/productController');

/* Product API Collection */
router.get('/products', product.all);
router.get('/products/available', product.available);
router.get('/product/find', product.findOne);
router.post('/create', product.create);
router.put('/update/:id', product.update);
router.delete('/delete/:id', product.delete)


/* User API Collection */
module.exports = router;