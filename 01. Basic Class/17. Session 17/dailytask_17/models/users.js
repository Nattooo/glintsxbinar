'use strict';
const bcrypt = require('bcrypt');

module.exports = (sequelize, DataTypes) => {
  const Users = sequelize.define('Users', {
    email: {
      type: DataTypes.STRING,
      unique: true,
      validate: {
        isEmail: true,
        isLowercase: true
      }
    },
    encrypted_password: {
      type: DataTypes.STRING,
      validate: {
        min: 6
      }
    }  
  }, {
    hooks: {
      // biar beforesave di lowercase, tapi lupa kenapa disini jg, crash dimana codingan nya..
      // order on hooks is important
      beforeValidate: instance => {
        instance.email = instance.email.toLowerCase();
      },

      beforeCreate: instance => {
        
        // Change the encrypted_password into hashed encrypted_password
        instance.encrypted_password = bcrypt
          .hashSync(instance.encrypted_password, 10)
      }
    }
  });

  /*

  user.beforeCreate(instance => {       
    // Change the encrypted_password into hashed encrypted_password
    instance.encrypted_password = bcrypt
      .hashSync(instance.encrypted_password, 10)
  })

  */

  Users.associate = function(models) {
    // associations can be defined here
  };
  return Users;

};