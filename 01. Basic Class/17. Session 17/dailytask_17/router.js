const router = require('express').Router();
/* penjabaran dari sini
const express = require('express');
const router = express.Router();
*/

const productsController = require('./controllers/productscontroller');
const usersController = require('./controllers/userscontroller');
const authorization = require('./authorization');

/* Product API Collection */
router.post('/products', authorization.logicToken, productsController.create);
router.get('/products', productsController.show);
router.get('/products/available', productsController.available);
router.get('/products/:id', productsController.findById);
router.put('/products/:id', authorization.logicToken, productsController.update);

router.post('/users/register', usersController.register);
router.post('/users/login', usersController.login);
router.get('/users/me', usersController.me);

module.exports = router;
