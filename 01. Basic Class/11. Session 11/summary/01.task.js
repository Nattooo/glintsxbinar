Array.prototype.sample = function() {
    return this[
      Math.floor(
        Math.random() * this.length
      )
    ]
  }
  
class Human {
    constructor(props) {
        let { name, address, lang } = props;
        this.name = name;
        this.address = address;
        this.lang = lang;    
}

    static isLivingInEarth = true;
    introduce() {
        console.log(`Hi, my name is ${this.name}`);
    }
    cook() {
        console.log(`I like to cook`)
    }
}

class chef extends Human {
    constructor(props) {
        super(props);
        this.cuisines = props.cuisines;
        this.types = props.types;
    }

    foods() {
        console.log(`let's make this delicious ${this.cuisines.sample()}`)
    }

    foodTypes() {
        console.log(`it's kind of ${this.types} cuisines`)
    }

    promotes() {
        console.log(`promoting restaurant with digital marketing`)
    }

    cook() {
        super.cook();
        console.log(`I have a better skill in my expertise.`)
    }

    introduce() {
        console.log(`Hi!, I am ${this.types} chef`)
    }

}

const Natalia = new chef ({
    name: "Natalia",
    address: "Madiun",
    lang: "Indonesian",
    cuisines: ["spagetti carbonara", "pizza"],
    types: "Italian"
})

Natalia.foodTypes();