class Human {
    constructor(name, isMarried, lang, nationality) {
        this.name = name;
        this.isMarried = isMarried;
        this.lang = lang;
        this.nationality = nationality;
    }

    greet(anotherHuman) {
        console.log(`Hi ${anotherHuman.name}, My name is ${this.name} `)
    }

    introduce () {
        console.log(`
        So, I'm ${this.nationality},
        I speak ${this.lang},
        and I'm ${this.isMarried ? "Single" : "Married"}
        `);
    }

    canMarry(anotherHuman) {
        if (this.lang === anotherHuman.lang) {
            console.log(`Hey ${anotherHuman.name}. Let's get to know each other`);
            if (this.isMarried === anotherHuman.isMarried) {
                console.log(`So, since you're single too. Let's get married!`);
            } else {
                console.log(`Oh, you're married. I'm brokenhearted`)
            }
        } else {
            console.log(`Let's be friend ${anotherHuman.name}. let me learn ${anotherHuman.lang}, then I will propose to you`)
        }
    }
}

const Nana = new Human("Nana", "false", "Indonesia", "Indonesian");

const White = new Human("White", "false", "Indonesia", "Australian");

White.greet(Nana);
White.introduce()
White.canMarry(Nana);
