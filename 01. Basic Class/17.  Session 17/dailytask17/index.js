const express = require('express');
const app = express();
const router = require('./router.js');  //Middleware

app.use(express.json());  //Middleware to Parse JSON
app.use('/', router);

// GET /
app.get('/', function (req, res) {
    res.json({
        status: true,
        message: "Hello World!"
    })
})

app.listen(6000, () => {
    console.log("Listening on port 6000!");
})