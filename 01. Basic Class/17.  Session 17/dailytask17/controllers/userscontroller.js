const { Users } = require('../models').users;
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

module.exports = {

  register(req, res) {
    Users.create({
      email: req.body.email,
      encrypted_password: req.body.password
    })
      .then(user => {
        res.status(201).json({
          status: 'success',
          data: {
            user
          }
        })
      })
      .catch(err => {
        res.status(422).json({
          status: "failed",
          errors: [err.message]
        })
      })
  },

  login(req, res) {
    // console.log(token);
    Users.findOne({
      where: { email: req.body.email.toLowerCase() }
    })
      .then(instance => {
        if (!instance)
          return res.status(401).json({
            status: 'failed',
            errors: ["Email doesn't exist"]
          })

          console.log(instance.encrypted_password);
          console.log(req.body.password)

        let isPasswordValid = bcrypt.compareSync(req.body.password, instance.encrypted_password);

        if (!isPasswordValid)
          return res.status(401).json({
            status: 'failed',
            errors: ["Wrong password!"]
          })

        const token = jwt.sign({
          id: instance.id,
          email: instance.email
        }, 'RAHASIA')

        res.status(201).json({
          status: 'success',
          data: {
            token
          }
        })
      })
      .catch(err => {
        res.status(422).json({
          status: 'failed',
          errors: [err.message]
        })
      })
  },

  me(req, res) {
 // console.log(req.headers);
    let token = req.headers.authorization;
    let payload;
 
    try {
        payload = jwt.verify(token, 'RAHASIA');
        // console.log(payload.id);
    }
    catch(err){
        res.status(401).json({
            status: "failed",
            data: ['Invalid token']
        })
    }
    Users.findByPk(payload.id)
    .then(instance => {
        res.status(200).json({
            status: "success",
            data: {
                user: instance
            }
            })
    })
  }

}
