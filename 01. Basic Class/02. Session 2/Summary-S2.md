#Internet

Internet adalah jaringan yang menghubungkan satu device ke device yang lain.

#Cara Device berkomunikasi antara satu dengan lainnya

Setiap device memiliki IP Adress. Mereka membawa pesan untuk disampaikan kepada device yang dituju.
Pertama dari wifi di salurkan ke ISP (provider) lalu mereka perlu mencari IP Adress device yang dituju dengan cara bertanya kepada DNS.
Jalan yang dilakukan oleh pesan tersebut dinamakan route. Route diperlukan supaya tidak terjadi tabrakan antar pesan.
Apabila terjadi tabrakan maka mereka akan menunggu dengan waktu tertentu untuk dikirimkan kembali.
Setelah mengetahui dimana IP Adress device yang dituju, maka pesan akan disampaikan ke device tujuan tersebut.
Kemudian device yang dituju akan mengirim respon atau pesan kembali sesuai permintaan device pengirim.

#IP Address

IP Address atau Internet Protocol Address adalah identitas dari suatu device ketika device tersebut mengirimkan pesan.
There are 2 version of IP Address. They are IPv4 and IPv6. The newest version is IPv6. 
IPv6 dapat memberikan identitas yang lebih banyak dari pada IPv4. Hal tersebut dikarenakan IPv6 memiliki lebih banyak digit angka.

#Internet Transfer Protocol

Protocol dalam mengirimkan data dalam jaringan atau dari satu komputer ke komputer yang lain.

#HTTP

HTTP atau Hypertext Transfer Protocol adalah text yang digunakan untuk mengatur atau menampilkan isi dari data yang ingin ditampilkan di browser.
