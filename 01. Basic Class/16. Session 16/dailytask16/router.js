const express = require('express');
const router = express.Router();
const Products = require('./controller/productcontroller');
const Users = require('./controller/usercontroller.js')

router.get('/products', Products.all);
router.post('/create', Products.create);
router.get('/show', Products.show);
router.post('/register', Users.register);
router.post('/login', Users.login);





module.exports = router;