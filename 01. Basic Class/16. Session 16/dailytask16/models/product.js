'use strict';
module.exports = (sequelize, DataTypes) => {
  const product = sequelize.define('Products', {
    name: DataTypes.STRING,
    price: DataTypes.FLOAT,
    stock: DataTypes.INTEGER
  }, {});
  product.associate = function(models) {
    // associations can be defined here
  };
  return product;
};