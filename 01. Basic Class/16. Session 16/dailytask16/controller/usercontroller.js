const { user } = require('../models');
const bcrypt = require('bcryptjs');

module.exports = {
    register(req, res) {
        const salt = bcrypt.genSaltSync(10);
        const hash = bcrypt.hashSync(req.body.encrypted_password, salt);
    
        user.create({
            email: req.body.email.toLowerCase(),
            encrypted_password: hash
        })
        .then(user => {
            res.status(200).json({
                status:"success",
                data: user
            })
        })
        .catch(err => {
            res.status(422).json({
                status: "fail",
                errors: [err.message]
            })
        })
    },

    login(req, res) {
        user.findOne({
            where: {email: req.body.email.toLowerCase()}
        })
        .then(user => {
            if (bcrypt.compareSync(req.body.encrypted_password, user.encrypted_password)) {
                res.status(200).json({
                    status:"success",
                    message: "successfuly login"
                })
            }
            else {
                res.status(400).json({
                    message: "Input email and password incorectly"
                })
            }
        })
        .catch(err => {
            res.status(500).json({
                message: `server error: ${err}`
            })
        })
    }

}