const { Products } = require('../models');

module.exports = {

    all(req, res) {
        Products.findAll().then(data => {
          res.status(200).json({
            status: 'success',
            data: {
              Product: data
            }
          })
        })
      },

      create(req, res) {
        Products.create(req.body)
          .then(data => {
            res.status(201).json({
              status: "success",
              data: {
                Product: data
              }
            });
          })
          .catch(err => {
            res.status(422).json({
              status: 'fail',
              errors: [err.message]
            });
          })
      },
      
      show(req, res) {
        Products.findOne({
          where: {
            id: req.query.id
          }
        })
          .then(Products => {
            res.status(200).json({
              status: 'success',
              data: {
                Product
              }
            })
          })
      },
    

}