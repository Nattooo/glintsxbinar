const router = require('express').router();
const userController = require('./controllers/usersController');
const authenticate = require('./middlewares/authenticate');
const postController = require('./controllers/postsController');

//users
router.post('/user/register',userController.register);
router.post('/user/login',userController.login);

//posts
router.post('/post/create', authenticate,postController.create);
router.get('/post/show', postController.show);
router.post('post/approve', authenticate,postController.approve);
router.put('/post/:id', authenticate, postController.update);
router.delete('/post/:id', authenticate, postController.delete);

module.exports = router