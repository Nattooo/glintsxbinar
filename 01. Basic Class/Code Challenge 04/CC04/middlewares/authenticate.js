const jwt = require('jsonwebtoken');
const { Users } = require('../models');


module.exports = async (req, res, next) => {
         try {
            let token = req.headers.authorization; 
            let payload = await jwt.verify(token, 'rahasia');
            
            // Find the user instance and assign to the req.user
            let user = await Users.findByPk(payload.id)
            req.user = user;
            next();
        }
        catch(err) {
            return res.status(401).json({
                status: "fail",
                errors: [
                    "invalid Token"
                ]
            })
        }
    }
