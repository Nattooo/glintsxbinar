const { Posts: Post } = require('../models');
const success = require('../middlewares/successHandler');

exports.create = async function(req, res, next) {
    try {
        if (req.instance.role === 'admin') throw `Can't create post`;
        const post = await Post.create({
            title: req.body.title,
            body: req.body.body,
            user_id: req.user.id,
            approved: false
        });
        success(res, 201, post, 'post');
    }
    catch(err) {
        res.status(422);
        next(err)
    }
}

exports.show = async function(req, res, next) {
    try {
        const post;
        if (req.instance.role == 'admin') post = await Post.findAll({
            include: 'author'
        });
        if (req.instance.role == 'member') post = await Post.findAll({
            where: {
                approved: true,
                include: 'author'
            }
        })
        success(res, 201, post, 'post');
    }
    catch(err) {
        res.status(422);
        next(err)
    }
}

exports.approve = async function(req, res, next) {
    try {
        if (req.instance.role !== 'admin') throw `Can't update approval`;
        const post = await Post.update({
            approved: true
        }, {
            where: {
                id: req.params.PostId
            }
        })
        success(res, 201, post, 'post');
    }
    catch(err) {
        res.status(422);
        next(err)
    }
}

exports.update = async function(req, res, next) {
    try {
        if (req.instance.role === 'admin') throw `Can't edit posts`;
        const post = await Post.update(req.body, {
            where: { id: req.params.id }
        })
        success(res, 201, post, 'post');
    }
    catch(err) {
        res.status(422);
        next(err)
    }
}

exports.delete = async function(req, res, next) {
    try {
        const post = await Post.findByPk(req.params.postId);
        if (req.instance.role === 'member')
        await Post.destroy({
            where: {
                id: req.params.postId
            }
        });
        if (req.instance.role === 'admin') await Post.destroy;
        success(res, 201, post, 'post');
    }
    catch(err) {
        res.status(422);
        next(err)
    }

}