'use strict';
module.exports = (sequelize, DataTypes) => {
  const Posts = sequelize.define('Posts', {
    title: DataTypes.STRING,
    body: DataTypes.TEXT,
    user_id: DataTypes.INTEGER
  }, {});
  Posts.associate = function(models) {
    // associations can be defined here
    Post.belongsTo(models.Users, {
      foreignKey: 'user_id',
      as: 'author'
    })
  };
  return Posts;
};