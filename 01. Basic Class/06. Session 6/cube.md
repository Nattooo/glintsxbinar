const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

console.clear();
console.log("Which one do you want to calculate?");
console.log(`
1. Cube Area
2. Cube Volume
`)

function cubeArea(s) {    
    if (typeof +s !== 'number') {
            throw new Error('Input is not valid')
        } else {
            const hasil = 12 * s;
            return hasil;
        }
};

function cubeVolume(s) {
    if (typeof +s !== 'number') {
        throw new Error('Input is not valid')
    } else {
        const hasil = s ** 3;
        return hasil;
    }
};

function handleAnswer1() {
    console.clear();
    console.log("Calculate Cube Area");
    rl.question("Edge of the Cube: ", s => {
        console.log("Here's the result:", cubeArea(s))
        rl.close();
    })
}

function handleAnswer2() {
    console.clear();
    console.log("Calculate Cube Volume");
    rl.question("Edge of the Cube: ", s => {
        console.log("Here's the result:", cubeVolume(s))

        rl.close();
    })
}

rl.question('Answer: ', answer => {
    if (answer == 1) {
        handleAnswer1()
    }

    else if (answer == 2) {
        handleAnswer2()
    }

    else {
        console.log("Sorry, there's no option for that!")
        rl.close()
    }
});
