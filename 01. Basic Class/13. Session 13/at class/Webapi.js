var http = require('http');

//create a server object:
//http.createServer(function (req, res) {
//res.write('Hello World!'); //write a response to the client
//res.end(); //end the response
//}).listen(8080); //the server object listens on port 8080 

const PORT = 3000;
const app = http.createServer((req, res) => {
    //console.log(req.url);
    //res.writeHead(200);
    switch(req.url) {
        case '/':
            res.write("Hello World\n");
            break;

        case '/products':
            res.write("This is a product\n");
            break;
        
        default:
            res.writeHead(404);
            res.write("404 Not Found!\n")
    }

    res.write("Hello World\n");
    res.end();
});

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));
