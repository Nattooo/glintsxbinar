conts obj = {
    id: 1,
    name: "Fikri",
    address: "Solo"
} // Let's say we save it on locker A

const person = obj; // person only referencing that it's on locker A

// change the property

person.name = "another person"; // person name will change cz we are referencing on locker A
console.log(obj); // name: another person

// how to clone
const = {...obj} //using spread operator
CloseEvent.name = "Rizky";
console.log(clone);
console.log(obj);

//Clone and modify directly
const another = { ...obj, name: "natta", isMarried:false}
console.log(`another: `, another);
