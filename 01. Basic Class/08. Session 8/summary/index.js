const person = {
    name: "nata",
    address: "jalan graha manis, Madiun"
}

console.log(person.name); // Adrian
console.log(person.address); // jalan graha manis, Madiun

person.isMarried = true;
person["pets"] = ["cats","birds"] //using props name
person.gender = "male";


person[person.gender == "male" ? "wife" : "husband"] = {
    name: "someone",
    gender: person.gender == "male" ? "female" : "male"
}

console.log(person);